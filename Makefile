all: native web

web:
	mkdir -p bin/web
	em++ -Wall -Wextra -I include source/*.cpp -o bin/web/test.html -std=c++11

native:
	mkdir -p bin/native
	g++ -Wall -Wextra -I include source/*.cpp -lGL -lGLEW -lglut -o bin/native/native -std=c++11

run: native
	./bin/native/native