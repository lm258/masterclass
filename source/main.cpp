#define GLM_FORCE_RADIANS

#include <glm/glm.hpp>
#include <glm/gtc/matrix_transform.hpp>
#include <glm/gtc/type_ptr.hpp>
#include <GL/glew.h>
#include <GL/glut.h>
#include <stdio.h>
#include <cstring>
#include <vector>
#include <Plane.h>
#include <Shader.h>
#include <Boid.h>
#include <RenderTarget.h>
#include <cmath>
#include <LightShader.h>
#include <Flocking.h>

#define BOID_COUNT 100
#define MAP_WIDTH 0.495f

const std::string vertex_shader =
"attribute vec3 vPos;\n"
"attribute vec3 vColor;\n"
"attribute vec3 vNormal;\n"
"uniform mat4 model;\n"
"uniform mat4 view;\n"
"uniform mat4 proj;\n"
"uniform mat3 normalMatrix;\n"
"varying vec4 outColor;\n"
"varying vec4 outNormal;\n"
"varying vec4 outPos;\n"
"varying mat4 outModel;\n"
"varying mat4 outView;\n"
"varying float lightIntensity;"
"void main () {\n"
"  outColor          = vec4( vColor, 1.0 );"
"  outNormal         = vec4( normalize( normalMatrix*vNormal ), 1.0 );"
"  gl_Position       = proj * view * model * vec4( vPos, 1.0 );"
"  outPos            = gl_Position;"
"  outView           = view;"
"  outModel          = model;"
"}";

const std::string fragment_shader =
"uniform vec3 lightPos;\n"
"uniform vec3 lightColor;\n"
"varying vec4 outColor;\n"
"varying vec4 outNormal;\n"
"varying mat4 outModel;\n"
"varying mat4 outView;\n"
"varying vec4 outPos;\n"
"void main () {"
"  vec4 s               = normalize( (outView*vec4(lightPos,1.0)) - (outView*outModel*outPos) );"
"  float lightIntensity = max( dot( outNormal, s ), 0.1 );"
"  gl_FragColor         = outColor * vec4( lightIntensity, lightIntensity, lightIntensity, 1.0 );"
"}";

Plane* ground;
std::vector<Boid*> boidList;
Camera* camera;
LightShader* shader;
Flocking* flocking;

void draw_frame() {
    glClear( GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT );

    ground->render();
    for( std::vector<Boid*>::iterator it= boidList.begin() ; it != boidList.end(); ++it ) {
        (*it)->render();
    }
    flocking->flock( boidList );

    glutSwapBuffers();
}

int main(int argc, char **argv) {

    glutInit( &argc, argv );
    glutInitWindowSize( 800, 600 );
    glutInitDisplayMode( GLUT_DOUBLE | GLUT_RGB | GLUT_DEPTH );
    glutCreateWindow( "Flocking CuBoids" );
    glewInit();

    glutIdleFunc( draw_frame );
    glutDisplayFunc( draw_frame );
    glDepthMask( GL_TRUE );
    glEnable( GL_DEPTH_TEST );
    glDepthFunc( GL_LEQUAL );

    //Create camera
    camera = new Camera(
        glm::vec3(0.8f, 0.0f, 1.0f),
        glm::vec3(0.1f, 0.0f, 0.0f),
        glm::vec3(0.0f, 0.0f, 1.0f)
    );
    Model::setCamera( camera );

    //Create shaders
    shader = new LightShader( vertex_shader, fragment_shader );
    Model::setShader( shader );

    //Build boid VBO and VOA buffers
    Boid::build();

    //Create our group object
    ground  = new Plane();

    //Create flocking behaviour handler
    flocking = new Flocking( MAP_WIDTH, MAP_WIDTH );

    /*Create boids and place them on the map*/
    for( int i = 0; i < BOID_COUNT; i++ ) {

        float xR = ((float) rand()) / (float) RAND_MAX;
        float yR = ((float) rand()) / (float) RAND_MAX;

        xR = (xR * 2.0f) - 1.0f;
        yR = (yR * 2.0f) - 1.0f;

        Boid* boid = new Boid();
        boid->translate( glm::vec3( xR * MAP_WIDTH, yR * MAP_WIDTH, 0.02f ) );
        boidList.push_back( boid );
    }

    glutMainLoop(); 
}