#include <LightShader.h>

LightShader::LightShader( std::string vertex, std::string fragment )
    : Shader( vertex, fragment ) {
    lightPosition = glm::vec3( 0.0f, 50.0f, 50.0f );
    lightColor    = glm::vec3( 1.0f, 1.0f, 1.0f );
}

void LightShader::preRender( Camera* camera, glm::mat4 modelProjection ) {
    glUseProgram( this->program_ );

    //Set the model transformation
    GLint uniTrans = glGetUniformLocation( this->program_, "model");
    glUniformMatrix4fv( uniTrans, 1, GL_FALSE, glm::value_ptr( modelProjection ));

    //Set the camera matrix
    GLint uniView = glGetUniformLocation( this->program_, "view" );
    glUniformMatrix4fv( uniView, 1, GL_FALSE, glm::value_ptr( camera->getView() ) );

    GLint uniProj = glGetUniformLocation( this->program_, "proj" );
    glUniformMatrix4fv( uniProj, 1, GL_FALSE, glm::value_ptr( camera->getProjection() ) );

    //Set the normal matrix
    glm::mat3 normalMatrix = glm::inverseTranspose( glm::mat3( camera->getView()*modelProjection ) );
    GLint uniNormalMatrix = glGetUniformLocation( this->program_, "normalMatrix" );
    glUniformMatrix3fv( uniNormalMatrix, 1, GL_FALSE, glm::value_ptr( normalMatrix ) );

    GLint uniLightPos = glGetUniformLocation( this->program_, "lightPos" );
    glUniform3fv( uniLightPos, 1, glm::value_ptr( lightPosition ) );

    GLint uniLightColor = glGetUniformLocation( this->program_, "lightColor" );
    glUniform3fv( uniLightColor, 1, glm::value_ptr( lightColor ) );
}