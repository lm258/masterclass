#include <Shader.h>


GLuint Shader::getProgram() {
    return this->program_;
}

bool Shader::shaderError( GLuint shader ) {
    GLint status;
    glGetShaderiv( shader, GL_COMPILE_STATUS, &status );

    if( status == GL_FALSE ) {
        GLint infoLogLength;
        glGetShaderiv( shader, GL_INFO_LOG_LENGTH, &infoLogLength );

        GLchar* strInfoLog = new GLchar[infoLogLength + 1];
        glGetShaderInfoLog( shader, infoLogLength, NULL, strInfoLog );

        cerr << "Compilation error in shader" << strInfoLog << "\n";
        delete[] strInfoLog;
        return false;
    }
    return true;
}

void Shader::setTexture( GLuint position, GLuint texture, std::string name ) {
    glActiveTexture( GL_TEXTURE0 + position );
    glBindTexture( GL_TEXTURE_2D, texture );
    glUniform1i( glGetUniformLocation( this->program_, name.c_str() ), position );
}

Shader::Shader( std::string vertex, std::string fragment ) {

    #ifdef EMSCRIPTEN
        vertex = "#version 100\nprecision mediump float\n;" + vertex;
        fragment = "#version 100\nprecision mediump float\n;" + fragment;
    #else
        vertex = "#version 130\n" + vertex;
        fragment = "#version 130\n" + fragment;
    #endif

    const GLchar* vertex_raw[] = {
        (const GLchar*)vertex.c_str()
    };

    const GLchar* fragment_raw[] = {
        (const GLchar*)fragment.c_str()
    };

    GLuint vs = glCreateShader( GL_VERTEX_SHADER );
    glShaderSource( vs, 1, (const GLchar**)vertex_raw, NULL );
    glCompileShader( vs );

    if( Shader::shaderError( vs ) == false ) {
        cerr << "ERROR: Vertex shader failed to compile\n";
    };

    GLuint fs = glCreateShader( GL_FRAGMENT_SHADER );
    glShaderSource( fs, 1, (const GLchar**)fragment_raw, NULL );
    glCompileShader( fs );

    if( Shader::shaderError( fs ) == false ) {
        cerr << "ERROR: Fragment shader failed to compile\n";
    };

    this->program_ = glCreateProgram();
    glAttachShader( this->program_, fs );
    glAttachShader( this->program_, vs );
    glLinkProgram( this->program_ );
}


void Shader::preRender( Camera* camera, glm::mat4 modelProjection ) {
    glUseProgram( this->program_ );

    //Set model transformation matrix
    GLint uniTrans = glGetUniformLocation( this->program_, "model");
    glUniformMatrix4fv( uniTrans, 1, GL_FALSE, glm::value_ptr( modelProjection ));

    //Set the camera matrix
    GLint uniView = glGetUniformLocation( this->program_, "view" );
    glUniformMatrix4fv( uniView, 1, GL_FALSE, glm::value_ptr( camera->getView() ) );

    GLint uniProj = glGetUniformLocation( this->program_, "proj" );
    glUniformMatrix4fv( uniProj, 1, GL_FALSE, glm::value_ptr( camera->getProjection() ) );
}