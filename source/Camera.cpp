#include <Camera.h>

Camera::Camera( glm::vec3 translation, glm::vec3 center, glm::vec3 up ) {
    this->translation_ = translation;
    this->center_ = center;
    this->up_ = up;
    this->view_height_ = 600.0f;
    this->view_width_ = 800.0f;
    this->update();
}

void Camera::update() {
    this->view_ = glm::lookAt(
        this->translation_,
        this->center_,
        this->up_
    );

    this->projection_ = glm::perspective( glm::radians(45.0f), view_width_ / view_height_, 0.4f, 20.0f );
}

glm::mat4 Camera::getProjection() {
    return this->projection_;
}

glm::mat4 Camera::getView() {
    return this->view_;
}