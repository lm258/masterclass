#include <Flocking.h>

Flocking::Flocking( float mapWidth, float mapHeight ) {
    mapWidth_ = mapWidth;
    mapHeight_ = mapHeight;
}

glm::vec2 Flocking::calculateAlignment( Boid* boid, std::vector<Boid*> boidList ) {
    glm::vec2 v;
    int neighborCount = 0;
    for( auto i = boidList.begin(); i != boidList.end(); std::advance(i, 1) ) {
        if( (*i) != boid ) {
            if( boid->distance( (*i)->translate() ) < 300.0f ) {
                v.x += (*i)->velocity.x;
                v.y += (*i)->velocity.y;
                neighborCount++;
            }
        }
    }
    if( neighborCount == 0 ) {
        return v;
    }
    v.x /= neighborCount;
    v.y /= neighborCount;
    return v;
}

glm::vec2 Flocking::calculateCohesion( Boid* boid, std::vector<Boid*> boidList ) {
    glm::vec2 v;
    int neighborCount = 0;
    for( auto i = boidList.begin(); i != boidList.end(); std::advance(i, 1) ) {
        if( (*i) != boid ) {
            if( boid->distance( (*i)->translate() ) < 300.0f ) {
                v.x += (*i)->translate().x;
                v.y += (*i)->translate().y;
                neighborCount++;
            }
        }
    }
    if( neighborCount == 0 ) {
        return v;
    }
    v.x /= neighborCount;
    v.y /= neighborCount;
    return glm::normalize( glm::vec2( v.x - boid->translate().x, v.y - boid->translate().y ) );
}

glm::vec2 Flocking::calculateSeparation( Boid* boid, std::vector<Boid*> boidList ) {
    glm::vec2 v;
    int neighborCount = 0;
    for( auto i = boidList.begin(); i != boidList.end(); std::advance(i, 1) ) {
        if( (*i) != boid ) {
            if( boid->distance( (*i)->translate() ) < 300.0f ) {
                v.x += (*i)->translate().x - boid->translate().x;
                v.y += (*i)->translate().y - boid->translate().y;
                neighborCount++;
            }
        }
    }
    if( neighborCount == 0 ) {
        return v;
    }
    v.x /= neighborCount;
    v.y /= neighborCount;
    v.x *= -1;
    v.y *= -1;
    return glm::normalize( v );
}

glm::vec2 Flocking::boundryTest( Boid* boid ) {
    vec3 position = boid->translate();
    vec2 direction;
    if( position.x >= this->mapHeight_ ) {
        direction.x -= 1.0f;
    } else if( position.x <= -this->mapHeight_ ) {
        direction.x += 1.0f;
    } else if( position.y >= this->mapWidth_ ) {
        direction.y -= 1.0f;
    } else if( position.y <= -this->mapWidth_ ) {
        direction.y += 1.0f;
    }
    return direction;
}

void Flocking::flock( std::vector<Boid*> boidList ) {
    for( auto i = boidList.begin(); i != boidList.end(); std::advance(i, 1) ) {
        glm::vec2 alignment   = calculateAlignment( (*i), boidList );
        glm::vec2 cohesion    = calculateCohesion( (*i), boidList );
        glm::vec2 separation  = calculateSeparation( (*i), boidList );
        glm::vec2 boundryDir  = boundryTest( (*i) );

        (*i)->velocity.x += alignment.x * 0.100f + cohesion.x * 0.005f + separation.x * 0.005f;
        (*i)->velocity.y += alignment.y * 0.100f + cohesion.y * 0.005f + separation.y * 0.005f;
        
        (*i)->velocity.x += boundryDir.x * 0.05f;
        (*i)->velocity.y += boundryDir.y * 0.05f;
        (*i)->velocity = glm::normalize( (*i)->velocity ) * vec2( 0.005f, 0.005f );

        (*i)->translate( glm::vec3(
            (*i)->translate().x + (*i)->velocity.x,
            (*i)->translate().y + (*i)->velocity.y,
            0.0f
        ));
    }
}