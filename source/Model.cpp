#include <Model.h>


Camera* Model::camera_ = NULL;
Shader* Model::shader_ = NULL;

Model::Model() {
    this->scale_ = glm::vec3( 1.0f, 1.0f, 1.0f );
    this->translation_ = glm::vec3( 0.0f, 0.0f, 0.0f );
};

void Model::setCamera( Camera* camera ) {
    Model::camera_ = camera;
}

void Model::setShader( Shader* shader ) {
    Model::shader_ = shader;
}

std::vector<vec3> Model::calculateNormals( std::vector<vec3> vertices, std::vector<GLushort> index ) {
    std::vector<vec3> normals( vertices.size(), vec3( 0.0f, 0.0f, 0.0f ) );

    for( auto i = index.begin(); i != index.end(); std::advance(i, 3) ) {
        vec3 v[3] = { vertices[*i], vertices[*(i+1)], vertices[*(i+2)] };
        vec3 normal = glm::cross(v[1] - v[0], v[2] - v[0]);

        for (int j = 0; j < 3; ++j) {
            vec3 a = v[(j+1) % 3] - v[j];
            vec3 b = v[(j+2) % 3] - v[j];
            float weight = acos(glm::dot(a, b) / (a.length() * b.length()));
            normals[*(i+j)] += weight * normal;
        }
    }

    for( unsigned int k = 0; k < normals.size(); k++ ) {
        normals[k] = glm::normalize( normals[k] );
    }

    return normals;
}

void Model::updateTransformationMatrix() {
    this->transformation_ = glm::translate(
        glm::mat4( 1.0f ),
        this->translation_
    );

    this->transformation_ = glm::rotate(
        this->transformation_,
        this->rotation_.x,
        glm::vec3( 1.0f, 0.0f, 0.0f )
    );

    this->transformation_ = glm::rotate(
        this->transformation_,
        this->rotation_.y,
        glm::vec3( 0.0f, 1.0f, 0.0f )
    );

    this->transformation_ = glm::rotate(
        this->transformation_,
        this->rotation_.z,
        glm::vec3( 0.0f, 0.0f, 1.0f )
    );

    this->transformation_ = glm::scale(
        this->transformation_,
        this->scale_
    );
};

void Model::translate( glm::vec3 translation ) {
    this->translation_ = translation;
    this->updateTransformationMatrix();
}

void Model::scale( glm::vec3 scale ) {
    this->scale_ = scale;
    this->updateTransformationMatrix();
}

void Model::rotate( glm::vec3 rotation ) {
    this->rotation_ = rotation;
    this->updateTransformationMatrix();
}

float Model::distance( glm::vec3 position ) {
    return glm::distance( this->translation_, position ); 
}

glm::vec3 Model::translate() {
    return this->translation_;
}

glm::vec3 Model::scale() {
    return this->scale_;
}

void Model::render() {
    if( shader_ != NULL && camera_ != NULL ) {
        shader_->preRender( camera_, this->transformation_ );
    }
}