#include <Boid.h>

const std::vector<vec3> Boid::shape = {
    vec3(-1.0f, -1.0f,  1.0f), 
    vec3( 1.0f, -1.0f,  1.0f), 
    vec3( 1.0f,  1.0f,  1.0f), 
    vec3(-1.0f,  1.0f,  1.0f), 
    vec3(-1.0f, -1.0f, -1.0f), 
    vec3( 1.0f, -1.0f, -1.0f), 
    vec3( 1.0f,  1.0f, -1.0f), 
    vec3(-1.0f,  1.0f, -1.0f) 
};

const std::vector<vec3> Boid::color = {
    vec3( 1.0f,  0.5f,  0.5f), 
    vec3( 1.0f,  0.5f,  0.5f), 
    vec3( 1.0f,  0.5f,  0.5f), 
    vec3( 1.0f,  0.5f,  0.5f), 
    vec3( 1.0f,  0.5f,  0.5f),
    vec3( 1.0f,  0.5f,  0.5f), 
    vec3( 1.0f,  0.5f,  0.5f), 
    vec3( 1.0f,  0.5f,  0.5f) 
};

std::vector<vec3> Boid::normal = {
};

const std::vector<GLushort> Boid::index = {
    0, 1, 2, 2, 3, 0, 
    3, 2, 6, 6, 7, 3,
    7, 6, 5, 5, 4, 7, 
    4, 0, 3, 3, 7, 4, 
    0, 1, 5, 5, 4, 0,
    1, 5, 6, 6, 2, 1
};

GLuint Boid::vbo[3];
GLuint Boid::vao;
GLuint Boid::vio;

Boid::Boid() {
    this->scale( glm::vec3( 0.005f, 0.005f, 0.005f ) );
};

void Boid::build() {
    //Create vertex array to store VBO
    glGenVertexArrays( 1, &Boid::vao );
    glBindVertexArray( Boid::vao );

    //Generate 3 buffers
    glGenBuffers( 3, Boid::vbo );

    //Bind vertex data
    glBindBuffer( GL_ARRAY_BUFFER, Boid::vbo[0] );
    glBufferData( GL_ARRAY_BUFFER, sizeof(vec3) * Boid::shape.size(), &Boid::shape[0], GL_STATIC_DRAW );
    glVertexAttribPointer( 0, 3, GL_FLOAT, GL_FALSE, 0, NULL );
    glEnableVertexAttribArray( 0 );

    //Bind color data
    glBindBuffer( GL_ARRAY_BUFFER, Boid::vbo[1] );
    glBufferData( GL_ARRAY_BUFFER, sizeof(vec3) * Boid::color.size(), &Boid::color[0], GL_STATIC_DRAW );
    glVertexAttribPointer( 1, 3, GL_FLOAT, GL_FALSE, 0, NULL );
    glEnableVertexAttribArray( 1 );

    //Bind normal data
    Boid::normal = Model::calculateNormals( Boid::shape, Boid::index );

    glBindBuffer( GL_ARRAY_BUFFER, Boid::vbo[2] );
    glBufferData( GL_ARRAY_BUFFER, sizeof(vec3) * Boid::normal.size(), &Boid::normal[0], GL_STATIC_DRAW );
    glVertexAttribPointer( 2, 3, GL_FLOAT, GL_FALSE, 0, NULL );
    glEnableVertexAttribArray( 2 ); 

    //Create our element index buffer
    glGenBuffers( 1, &Boid::vio );
    glBindBuffer( GL_ELEMENT_ARRAY_BUFFER, Boid::vio );
    glBufferData( GL_ELEMENT_ARRAY_BUFFER, Boid::index.size() * sizeof(GLushort), &Boid::index[0], GL_STATIC_DRAW );
    
    //Disable bound VAO
    glBindVertexArray(0);
}

void Boid::render() {
    Model::render();
    glBindVertexArray( Boid::vao );
    glBindBuffer( GL_ELEMENT_ARRAY_BUFFER, Boid::vio );

    glDrawElements( GL_TRIANGLES, Boid::index.size(), GL_UNSIGNED_SHORT, (void*)0  );

    glBindBuffer( GL_ELEMENT_ARRAY_BUFFER, 0 );
    glBindVertexArray( 0 );
}