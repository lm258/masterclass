#include <RenderTarget.h>


RenderTarget::RenderTarget( float width, float height ) {

    glGenFramebuffers( 1, &this->framebuffer_ );
    glBindFramebuffer( GL_FRAMEBUFFER, this->framebuffer_ );

    glGenTextures( 1, &this->texture_color_ );
    glBindTexture( GL_TEXTURE_2D, this->texture_color_ );
    glTexParameteri( GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR );
    glTexParameteri( GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR );
    glTexParameteri( GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_CLAMP );
    glTexParameteri( GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_CLAMP );
    glTexImage2D( GL_TEXTURE_2D, 0, GL_RGBA8, width, height, 0, GL_RGBA, GL_UNSIGNED_BYTE, 0 );

    glGenTextures( 1, &this->texture_depth_ );
    glBindTexture( GL_TEXTURE_2D, this->texture_depth_ );
    glTexParameteri( GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR );
    glTexParameteri( GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR );
    glTexParameteri( GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_CLAMP );
    glTexParameteri( GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_CLAMP );
    glTexImage2D( GL_TEXTURE_2D, 0, GL_DEPTH_COMPONENT24, width, height, 0, GL_DEPTH_COMPONENT, GL_FLOAT, 0 );

    
    glFramebufferTexture2D( GL_FRAMEBUFFER, GL_COLOR_ATTACHMENT0 + 0, GL_TEXTURE_2D, this->texture_color_, 0 );
    glFramebufferTexture2D( GL_FRAMEBUFFER, GL_DEPTH_ATTACHMENT, GL_TEXTURE_2D, this->texture_depth_, 0 );

    //add attachements
    GLenum drawBuffer[1] = { GL_COLOR_ATTACHMENT0 + 0 };
    glDrawBuffers( 1, drawBuffer );

    //Check for FBO completeness
    if( glCheckFramebufferStatus( GL_FRAMEBUFFER ) != GL_FRAMEBUFFER_COMPLETE ){
        std::cout << "Error! FrameBuffer is not complete\n";
    }

    glBindFramebuffer( GL_FRAMEBUFFER, 0 );
}

GLuint RenderTarget::getColorTexture() {
    return this->texture_color_;
}

GLuint RenderTarget::getDepthTexture() {
    return this->texture_depth_;
}

void RenderTarget::bind(){
    glBindFramebuffer( GL_FRAMEBUFFER, this->framebuffer_ );
}

//unbind framebuffer from pipeline. We will call this method in the render loop
void RenderTarget::unbind() {
    glBindFramebuffer( GL_FRAMEBUFFER, 0 );
}