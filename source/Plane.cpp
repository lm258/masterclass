#include <Plane.h>

const std::vector<vec3> Plane::shape = {
    //First Triangle
    vec3(-0.5f,  0.5f,  0.0f),
    vec3( 0.5f,  0.5f,  0.0f),
    vec3(-0.5f, -0.5f,  0.0f),

    //Second Triangle
    vec3( 0.5f,  0.5f,  0.0f),
    vec3( 0.5f, -0.5f,  0.0f),
    vec3(-0.5f, -0.5f,  0.0f)
};

const std::vector<vec3> Plane::color = {
    //First Triangle
    vec3( 0.4f,  0.6f,  0.6f ),
    vec3( 0.4f,  0.6f,  0.6f ),
    vec3( 0.4f,  0.6f,  0.6f ),

    //Second Triangle
    vec3( 0.4f,  0.6f,  0.6f ),
    vec3( 0.4f,  0.6f,  0.6f ),
    vec3( 0.4f,  0.6f,  0.6f )
};

std::vector<vec3> Plane::normal = {
    //First Triangle
    vec3(-0.5f,  0.5f,  1.0f),
    vec3( 0.5f,  0.5f,  1.0f),
    vec3(-0.5f, -0.5f,  1.0f),

    //Second Triangle
    vec3( 0.5f,  0.5f,  1.0f),
    vec3( 0.5f, -0.5f,  1.0f),
    vec3(-0.5f, -0.5f,  1.0f)
};

const std::vector<GLushort> Plane::index = {
    0, 1, 2, 3, 4, 5
};

GLuint Plane::vio;

Plane::Plane() {
    //Create vertex array to store VBO
    glGenVertexArrays( 1, &this->vao );
    glBindVertexArray( this->vao );

    //Generate 3 buffers
    glGenBuffers( 3, this->vbo );

    //Bind vertex data
    glBindBuffer( GL_ARRAY_BUFFER, this->vbo[0] );
    glBufferData( GL_ARRAY_BUFFER, Plane::shape.size() * sizeof(vec3), &Plane::shape[0], GL_STATIC_DRAW );
    glVertexAttribPointer( 0, 3, GL_FLOAT, GL_FALSE, 0, NULL );
    glEnableVertexAttribArray( 0 );

    //Bind color data
    glBindBuffer( GL_ARRAY_BUFFER, this->vbo[1] );
    glBufferData( GL_ARRAY_BUFFER, Plane::color.size() * sizeof(vec3), &Plane::color[0], GL_STATIC_DRAW );
    glVertexAttribPointer( 1, 3, GL_FLOAT, GL_FALSE, 0, NULL );
    glEnableVertexAttribArray( 1 );

    //Bind normal data
    glBindBuffer( GL_ARRAY_BUFFER, this->vbo[2] );
    glBufferData( GL_ARRAY_BUFFER, Plane::normal.size() * sizeof(vec3), &Plane::normal[0], GL_STATIC_DRAW );
    glVertexAttribPointer( 2, 3, GL_FLOAT, GL_FALSE, 0, NULL );
    glEnableVertexAttribArray( 2 );

    //Create our element index buffer
    glGenBuffers( 1, &Plane::vio );
    glBindBuffer( GL_ELEMENT_ARRAY_BUFFER, Plane::vio );
    glBufferData( GL_ELEMENT_ARRAY_BUFFER, Plane::index.size() * sizeof(GLushort), &Plane::index[0], GL_STATIC_DRAW );
  
    //Disable bound VAO
    glBindVertexArray(0);
}

void Plane::render() {
    Model::render();
    glBindVertexArray( this->vao );
    glBindBuffer( GL_ELEMENT_ARRAY_BUFFER, Plane::vio );

    glDrawElements( GL_TRIANGLES, Plane::index.size(), GL_UNSIGNED_SHORT, (void*)0  );

    glBindBuffer( GL_ELEMENT_ARRAY_BUFFER, 0 );
    glBindVertexArray( 0 );
}