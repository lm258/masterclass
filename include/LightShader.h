#include <Shader.h>

class LightShader : public Shader {

    glm::vec3 lightPosition;
    glm::vec3 lightColor;

public:

    LightShader( std::string vertex, std::string fragment );

    void preRender( Camera* camera, glm::mat4 modelProjection );
};