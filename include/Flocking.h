/*Flocking implementation class*/
#include <vector>
#include <Boid.h>

class Flocking {
    float mapWidth_;
    float mapHeight_;

public:
    Flocking( float mapWidth, float mapHeight );

    glm::vec2 calculateAlignment( Boid* boid, std::vector<Boid*> boidList );
    glm::vec2 calculateCohesion( Boid* boid, std::vector<Boid*> boidList );
    glm::vec2 calculateSeparation( Boid* boid, std::vector<Boid*> boidList );
    glm::vec2 boundryTest( Boid* boid );
    void flock( std::vector<Boid*> boidList );
};