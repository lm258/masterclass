#include <GL/glew.h>
#include <glm/glm.hpp>
#include <glm/gtc/matrix_transform.hpp>
#include <glm/gtc/type_ptr.hpp>

#pragma once

class Camera {
private:
    glm::vec3 translation_;
    glm::vec3 center_;
    glm::vec3 up_;
    glm::mat4 projection_;
    glm::mat4 view_;
    float view_width_;
    float view_height_;

    void update();

public:
    Camera( glm::vec3 translation, glm::vec3 center, glm::vec3 up );

    glm::mat4 getProjection();
    glm::mat4 getView();

    void setShaderMatrix( GLuint shader = 0 );
};