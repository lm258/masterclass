#include <GL/glew.h>
#include <glm/glm.hpp>
#include <glm/gtc/matrix_transform.hpp>
#include <glm/gtc/type_ptr.hpp>
#include <vector>
#include <Shader.h>
#include <Camera.h>

#pragma once
using namespace glm;

class Model {
    static Camera* camera_;
    static Shader* shader_;
    glm::vec3 scale_;
    glm::vec3 translation_;
    glm::vec3 rotation_;
    glm::mat4 transformation_;

    void updateTransformationMatrix();

public:
    glm::vec2 velocity;
    
    Model();

    static void setCamera( Camera* camera );
    static void setShader( Shader* shader );

    static std::vector<vec3> calculateNormals( std::vector<vec3> vertices, std::vector<GLushort> index );

    void translate( glm::vec3 translation );
    void scale( glm::vec3 scale );
    void rotate( glm::vec3 rotation );
    
    float distance( glm::vec3 position );

    glm::vec3 translate();
    glm::vec3 scale();
    
    void render();
};