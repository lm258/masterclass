#include <GL/glew.h>
#include "glm/gtc/matrix_inverse.hpp"
#include <string>
#include <iostream>
#include <cstring>
#include <Camera.h>

using namespace std;

#pragma once

class Shader {

protected:
    GLuint program_;
    bool shaderError( GLuint shader );

public:
    Shader( std::string vertex, std::string fragment );

    void setTexture( GLuint position, GLuint texture, std::string name );

    GLuint getProgram();

    virtual void preRender( Camera* camera, glm::mat4 modelProjection );
};