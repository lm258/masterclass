#include <GL/glew.h>
#include <glm/glm.hpp>
#include <glm/gtc/matrix_transform.hpp>
#include <glm/gtc/type_ptr.hpp>
#include <vector>
#include <Model.h>

#pragma once
using namespace glm;

class Plane : public Model {

private:
    GLuint vbo[3];
    GLuint vao;
    static GLuint vio;
    static const std::vector<vec3> shape;
    static const std::vector<vec3> color;
    static const std::vector<GLushort> index;

public:

    static std::vector<vec3> normal;
    Plane();
    void render();
};