#include <GL/glew.h>
#include <glm/glm.hpp>
#include <glm/gtc/matrix_transform.hpp>
#include <glm/gtc/type_ptr.hpp>
#include <iostream>

#pragma once

class RenderTarget {
    GLuint framebuffer_;
    GLuint texture_color_;         
    GLuint texture_depth_;   

public:

    RenderTarget( float width, float height );

    void bind();

    GLuint getColorTexture();

    GLuint getDepthTexture();

    void unbind();
};